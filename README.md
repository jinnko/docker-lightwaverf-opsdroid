# LightwaveRF Call for Heat ChatOps

This bot works in conjunction with https://github.com:jinnko/LightwaveRF-1.git

## Quickstart

1. Create your config in `config/configuration.yml`. It should look something like this:

    ```yaml
    welcome-message: false

    connectors:
        matrix:
            # Required
            mxid: '@YOUR_BOT_USERNAME:matrix.org'
            password: 'THE_BOT_PASSWORD'
            rooms:
                main: '#ROOM_ALIAS:matrix.org'
            enable_encryption: True
            store_path: '/home/opsdroid/.local/share/opsdroid/'

    databases:
        sqlite:
            path: '/home/opsdroid/.local/share/opsdroid/sqlite.db'
            table: 'opsdroid'

    skills:
        lightwaverf:
            path: '/home/opsdroid/.local/share/opsdroid/lightwaverf_skill.py'
    ```

2. Start your container:

    ```sh
    docker compose up -d
    ```

3. Request `help` in the chat room.
