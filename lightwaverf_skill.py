from opsdroid.skill import Skill
from opsdroid.matchers import match_regex
from prometheus_client.parser import text_string_to_metric_families
from pytz import timezone

from datetime import datetime
import parsedatetime
import requests
import socket
import time

from pprint import PrettyPrinter

pp = PrettyPrinter(indent=4)

http_client = "requests"
PROMETHEUS = "http://10.3.2.7:9191"
LWRF_IP = "10.3.2.7"
LWRF_PORT = 9761

c = parsedatetime.Constants()
c.BirthdayEpoch = 22
p = parsedatetime.Calendar(c)


class LightwaveRfSkill(Skill):
    @match_regex(r"[Hh]elp")
    async def help(self, message):
        await message.respond(
            """Here's the commands I understand:
            <ul>
                <li><code>status</code>: get the last known status of all the TRVs</li>
                <li><code>set away</code>: disable Call for Heat indefinitely</li>
                <li><code>set away until ...</code>: disable Call for Heat until a given time</li>
                <li><code>set home</code>: enable Call for Heat</li>
                <li><code>restart</code>: trigger a restart of the LWRF CFH app</li>
            </ul>
        """
        )

    @match_regex(r"[Rr]estart")
    async def restart_lwrfcfh(self, message):
        try:
            lwrf_command = "?M=TriggeredByOpsdroid"

            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.sendto(bytes(lwrf_command, "ascii"), (LWRF_IP, LWRF_PORT))

            await message.respond(f"Request to restart LWRF CFH app sent.")
        except Exception as e:
            await message.respond(f"Unexpected error requesting restarting: {e}")

        return

    @match_regex(r"[Ss]et (home|away).*")
    async def set_away_state(self, message):
        words = message.text.lower().split(" ")
        requested_run_mode = words[1]

        # Use transaction value of '-1' because we can't predict the transactions being handled by lwrf/cfh
        lwrf_command = (
            '*!{"trans": '
            + str(-1)
            + ', "action": "runmode"'
            + ', "mode": "'
            + requested_run_mode
            + '"'
        )

        try:
            if len(words) > 2:
                until, err = p.parseDT(
                    datetimeString=" ".join(words[2:]), tzinfo=timezone("Europe/London")
                )

                date_epoch = until.timestamp()
                lwrf_command += ', "until": ' + str(int(date_epoch))

                date_str = until.strftime("%Y-%m-%d %H:%M %Z")
                await message.respond(
                    f"I think you mean to set <strong>{requested_run_mode}</strong> mode until {date_str}.  If that's not right just try again."
                )

            lwrf_command += "}"
        except Exception as e:
            await message.respond(f"Unexpected error parsing end time: {e}")
            return

        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.sendto(bytes(lwrf_command, "ascii"), (LWRF_IP, LWRF_PORT))

            await message.respond(
                f"Request to set <strong>{requested_run_mode}</strong> mode has been sent.  Wait a moment the request status to confirm."
            )
        except Exception as e:
            await message.respond(f"Unexpected error setting away state: {e}")

        return

    @match_regex(r"[Ss]tatus")
    async def status(self, message):
        try:
            text, err = await self.get_prometheus()
            if err is not None:
                await message.respond(f"{pp.pformat(err)}")
                return
        except Exception as e:
            await message.respond(f"Unexpected error getting prometheus data: {e}")
            return

        try:
            data, err = await self.parse_prometheus(text)
            if err is not None:
                await message.respond(f"{pp.pformat(err)}")
                return
        except Exception as e:
            await message.respond(
                f"Unexpected error parsing prometheus data: {pp.pformat(e)}"
            )
            return

        try:
            if "html" == "table":
                response = """<table><tr>
                    <th>Room</th>
                    <th>battery</th>
                    <th>temp</th>
                    <th>ratio</th>
                    <th>updated</th>
                </tr>"""
                for key, metrics in data.items():
                    if key != "lwl_run_mode":
                        response += (
                            "<tr>"
                            f"<th>{key}</th>"
                            f"<td>{metrics['battery_volts']}v</td>"
                            f"<td>{metrics['current_celsius']}°C / {metrics['target_celsius']}°C</td>"
                            f"<td>{metrics['output_ratio']} / {metrics['target_ratio']}</td>"
                            f"<td>{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(metrics['time']))}</td>"
                            "</tr>"
                        )
                    else:
                        response += (
                            f"<tr><td>Run mode</td><td colspan=4>{metrics}</td></tr>"
                        )
                response += "</table>"
            else:
                tmpl = "{:<10} {:<11} {:<5} {:<10}\n"
                response = "<pre><code>" + tmpl.format(
                    "Room", "temp (c/t)", "ratio", "updated"
                )
                run_mode = "Run mode:      unknown"
                boiler = "Boiler switch: unknown"
                for key, metrics in data.items():
                    if key == "lwl_run_mode":
                        run_mode = f"Run mode:    {metrics}"

                    elif key == "Boiler switch":
                        boiler = f"Boiler switch: {bool(metrics['output_ratio'])}"

                    else:
                        response += tmpl.format(
                            key.replace("Living room", "LR"),
                            str(metrics["current_celsius"])
                            + "°/"
                            + str(metrics["target_celsius"])
                            + "°",
                            str(int(metrics["output_ratio"] * 100)) + "%",
                            time.strftime(
                                "%H:%M %b %d ", time.localtime(metrics["time"])
                            ),
                        )

                response += f"\n{run_mode}\n{boiler}</code></pre>"

            await message.respond(response)

        except Exception as e:
            await message.respond(f"Unexpected error responding: {e}")

        return

    async def get_prometheus(self):
        if http_client == "aiohttp":
            async with aiohttp.ClientSession() as session:
                r = await session.get(PROMETHEUS + "/metrics")

            if r.status == 200:
                text = await r.text()
                status = r.status
            else:
                return None, f"Failed to get metrics from LWRF: r.status"

        elif http_client == "requests":
            r = requests.get(PROMETHEUS + "/metrics")

            if r.ok:
                text = r.text
                status = r.status_code
            else:
                return None, f"Failed to get metrics from LWRF: r.status_code"

        return text, None

    async def parse_prometheus(self, text):
        data = {}

        try:
            # print(f"text2: {text}", file=sys.stderr)
            for family in text_string_to_metric_families(text):
                for sample in family.samples:
                    if sample.name == "lwl_run_mode":
                        if (
                            sample.labels["lwl_run_mode"] == "home"
                            and sample.value == 1.0
                        ):
                            data["lwl_run_mode"] = "home"
                        elif (
                            sample.labels["lwl_run_mode"] == "away"
                            and sample.value == 1.0
                        ):
                            data["lwl_run_mode"] = "away"

                    elif sample.name.startswith("lwl_"):
                        if "name" in sample.labels:
                            if sample.labels["name"] in data:
                                data[sample.labels["name"]][
                                    sample.name.replace("lwl_", "")
                                ] = sample.value
                            else:
                                data[sample.labels["name"]] = {
                                    sample.name.replace("lwl_", ""): sample.value
                                }
            return data, None

        except Exception as e:
            return None, (e, text)
